**Thème du programme NSI choisi :** Base de données

**Notions liées :** Données en table de première

**Résumé de l’activité :** Activité débranchée en deux parties. Gestion de données inconvénients et avantages.

**Objectifs :** Découverte des bases de données relationnelles. Introduire le modèle relationnel sans trop de formalisation : relation, attribut, domaine, clef primaire, clef étrangère, schéma relationnel. 

**Auteur :** Stephane

**Durée de l’activité :** 2h 

**Forme de participation :** individuelle ou en binôme, en autonomie, collective 

**Matériel nécessaire :** Papier, stylo !

**Préparation :** Aucune

**Fiche élève cours :** https://gitlab.com/stephcel/mooc-2-ressources/-/blob/main/2.5_Evaluation-par-les-pairs/fiche_eleve_decouverte_BDR.md
**Fiche élève activité :** https://gitlab.com/stephcel/mooc-2-ressources/-/blob/main/2.5_Evaluation-par-les-pairs/media/traitement_donnees.md 

**Media pour l'activité :** https://gitlab.com/stephcel/mooc-2-ressources/-/tree/main/2.5_Evaluation-par-les-pairs/media

1. **fiche prof** : https://gitlab.com/stephcel/mooc-2-ressources/-/blob/main/2.5_Evaluation-par-les-pairs/fiche_prof_decouverte_BDR_stephane.md<br/>

2. **analyse d'intention** de l'activité élève : https://gitlab.com/stephcel/mooc-2-ressources/-/blob/main/2.5_Evaluation-par-les-pairs/fiche-analyse-nsi-premiere-donnee.md

3. une remédiation pour élèves en difficulté : https://gitlab.com/stephcel/mooc-2-ressources/-/tree/main/2.5_Evaluation-par-les-pairs/correction <br/>
**ou** une activité d'approfondissement : _lien_ <br/>
**ou** un mini projet : _lien_<br/>
!!! préciser le contexte et la nature de votre activité !!!

4. Evaluation 
- Si pour le point 3, vous avez choisi le mini projet, vous devrez proposer ici une **grille d'évaluation** adaptée : _lien_
- Sinon, proposer une évaluation de type **devoir sur table** : _lien_