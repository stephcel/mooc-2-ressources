import collections
from operator import attrgetter
import folium

def lire_donnees(nom_fichier_CSV, separateur = ';'):
    """
        lit le fichier csv dans le repertoire data et revoie la liste des donnees
        : param nom_fichier_CSV
        type str
        : param separateur , le séparateur utilisé dans le fichier csv. par défaut ;
        type str
        return dictionnaire 
        type dict
    """
    assert type(nom_fichier_CSV) is str,'nom_fichier_CSV is str'
    try :
        lecture = open(nom_fichier_CSV,'r',encoding = 'utf_8') # ouvre un canal en lecture vers text.txt
    except FileNotFoundError :
        raise # renvoie une erreur si le fichier n'existe pas
    toutes_les_lignes = lecture.readlines() # renvoie toutes les lignes du fichier dans la liste lignes
    for i in range(0,len(toutes_les_lignes)): 
        toutes_les_lignes[i] = toutes_les_lignes[i].rstrip("\n").split(separateur) # enleve les "\n" et convertit en list
    lecture.close() # ferme le canal en lecture
    convertir_num(toutes_les_lignes)
    return toutes_les_lignes

def convertir_num(liste_villes):
    '''
    convertit les valeurs numériques au format float ou int
    : param liste_villes liste de villes
    : effet de bord sur liste_villes
    '''
    for i in range(1, len(liste_villes)):  # on commence à la deuxième ligne, après la ligne des attributs
        liste_villes[i][2] = int(liste_villes[i][2] ) # cp
        liste_villes[i][3] = int(liste_villes[i][3] ) # population ...
        liste_villes[i][4] = float(liste_villes[i][4] ) # surf
        liste_villes[i][5] = float(liste_villes[i][5] ) # long
        liste_villes[i][6] = float(liste_villes[i][6] ) # latt
        liste_villes[i][7] = float(liste_villes[i][7] ) # altmin
        liste_villes[i][8] = float(liste_villes[i][8] ) # altmax
        




def creer_collection(liste_villes):
    '''
    créer une collection de tupple nommé à partir de la liste passée en paramètre.
    : param liste_villes liste de données chargées avec la fonction liste_donnees
    type list
    : return une collection de namedtuples
    type namedtuple    
    
    '''
    # creons un tuple nommé 'base_villes' avec les attributs adéquats
    base_villes = collections.namedtuple('base_villes',
                                         ['Dept', 'Nom', 'CP', 'Population',
                                          'Surface', 'Longitude', 'Latitude',
                                          'Alt_Min', 'Alt_Max'])
    collection = []
    for i in range(1, len(liste_villes)) :
        ville_namedtuple = base_villes(liste_villes[i][0], # dept
                                        liste_villes[i][1], # nom
                                        liste_villes[i][2], # cp
                                        liste_villes[i][3], # pop
                                        liste_villes[i][4], # surf
                                        liste_villes[i][5], # long
                                        liste_villes[i][6], # lat
                                        liste_villes[i][7], #altmin
                                        liste_villes[i][8] #altmax
                                        )
        collection.append(ville_namedtuple)
    return collection

        
def afficher_villes(collection_villes, nbre_villes = 10):
    '''
    affiche les nbre_villes premières villes de la liste des villes
    : param collection_villes
    type list of namedtuples
    : param nbre_villes par défaut 10
    type int
    '''
    try :
        for i in range(nbre_villes):
            print(collection_villes[i].Nom,
                  '(', collection_villes[i].Dept,
                  '/',
                  collection_villes[i].CP,')' ,
                  ' pop :' , collection_villes[i].Population,
                  ' surf:', collection_villes[i].Surface,
                  ' lat :', collection_villes[i].Latitude,
                  ' long:', collection_villes[i].Longitude,
                  ' alt-min:', collection_villes[i].Alt_Min,
                  ' alt-max:', collection_villes[i].Alt_Max
                  )
    except :
        print('fin de la collection')
              
def compter_villes(collection_villes, departement):
    '''
    renvoie le nombre de communes dans le departement précisé
    : param collection_villes
    type list of namedtuples
    : param departement le département
    type str
    : return le nbre de villes recherchées
    type int
    >>> liste_villes = lire_donnees('villes_metropole_corse.csv')
    >>> collection_villes = creer_collection(liste_villes)
    >>> compter_villes(collection_villes, '80')
    782
    '''
    nbre_villes = 0
    for ville in collection_villes :
        if ville.Dept == departement :
            nbre_villes += 1
    return nbre_villes
       
def compter_villes_selon_altitude(collection_villes, altitude, departement = ''):
    '''
    renvoie le nombre de communes dans le dpt précisé par défaut tous les dept.
    : param collection_villes
    type namedtuple
    : param altitude l'altitude de référence
    type float
    : param departement le département, par défaut tous les départements
    type str
    : return le nbre de villes recherchées
    type int
    >>> liste_villes = lire_donnees('villes_metropole_corse.csv')
    >>> convertir_num(liste_villes)
    >>> collection_villes = creer_collection(liste_villes)
    >>> compter_villes_selon_altitude(collection_villes, 10, '62')
    9
    '''
    nbre_villes = 0
    for ville in collection_villes :
        if ville.Alt_Max <= altitude :
            if departement == '' or (ville.Dept == departement) :
                nbre_villes += 1
    return nbre_villes

def trier(namedtuple_villes, attribut, decroissant = False):
    '''
    Tri le NamedTuple des villes selon l'attribut choisi
    : param  namedtuple_villes
    type list of namedTples
    : parma attribut : un attribut
    type str
    : param decroissant (par défaut false) ordre du tri
    : EFFET DE BORD SUR namedtuple_villes    
    '''
    namedtuple_villes.sort( key = attrgetter(attribut), reverse = decroissant)
    

def renvoyer_villes_sous_altitude(collection_villes, altitude):
    '''
    renvoie une collection de namedtuple correspondant aux villes située à une altitude inférieure à altitude
    :param collection_villes une collection de villes
    type namedtuple
    : param altitude l'altitude d référence
    type float
    : return la collection de ville vérifiant la condition
    type list of namedtuples
    '''
    collection_villes_choisies = []
    for ville in collection_villes :
        #ajout de la ville à la liste de villes sous condition
        if ville.Alt_Max <= altitude :
            collection_villes_choisies.append(ville)
    return collection_villes_choisies


def renvoyer_villes_selon_noms(collection, noms):
    '''
    renvoie une collection de ville correspondantes aux noms passés en paramètre
    : param collection liste des namedtuple de ville
    type liste 
    : param noms liste d enoms de villes en MAJUSCULE
    type liste
    : return une collection de villes
    type list    
    >>> liste_villes = lire_donnees('villes_metropole_corse.csv')
    >>> convertir_num(liste_villes)
    >>> collection = creer_collection(liste_villes)
    >>> renvoyer_villes_selon_noms(collection, ['CALAIS', 'LILLE'])
    [base_villes(Dept='59', Nom='LILLE', CP=59000, Population=227560, Surface=34.83, Longitude=3.06667, Latitude=50.6333, Alt_Min=17.0, Alt_Max=45.0), base_villes(Dept='62', Nom='CALAIS', CP=62100, Population=73636, Surface=33.5, Longitude=1.83333, Latitude=50.95, Alt_Min=0.0, Alt_Max=18.0)]
    '''
    collection_villes_choisies = []
    for ville in collection :
        #ajout de la ville à la liste de villes sous condition
        if ville.Nom in noms :
            collection_villes_choisies.append(ville)
    return collection_villes_choisies


def renvoyer_villes(collection, altitude, departement = '' , nbre_habitants = 0):
    '''
    renvoie une collection de ville correspondantes aux noms passés en paramètre
    : param collection liste des namedtuple de ville
    type liste 
    : param altitude
    type float
    : departement
    type str
    : param nbre_habitants
    type int
    : return une collection de villes
    type list    
    >>> liste_villes = lire_donnees('villes_metropole_corse.csv')
    >>> convertir_num(liste_villes)
    >>> collection_villes = creer_collection(liste_villes)
    >>> renvoyer_villes(collection_villes, 10, '62', 3000)
    [base_villes(Dept='62', Nom='COULOGNE', CP=62137, Population=5686, Surface=9.16, Longitude=1.88333, Latitude=50.9167, Alt_Min=0.0, Alt_Max=7.0)]
    '''
    collection_villes_choisies = []
    for ville in collection :
        #ajout de la ville à la liste de villes sous condition
        if (ville.Dept == departement or departement == '') and ville.Alt_Max < altitude and (ville.Population > nbre_habitants or nbre_habitants ==0) :
            collection_villes_choisies.append(ville)
    return collection_villes_choisies

def creer_liste_villes(collection_villes):
    '''
    renvoie une liste de villes à partir de la collection passée en paramètre
    : param collection_villes une collection de villes
    type list de namedtuples
    : return une liste de villes
    type list of list
    '''
    # premiere ligne avec les attributs
    liste_villes =  [['Dept', 'Nom', 'CP', 'Population','Surface', 'Longitude', 'Latitude','Alt_Min', 'Alt_Max']]
    # une boucle pourparcourir la collection et compléter la liste au fur et à mesure
    for ville in collection_villes :
      liste_villes.append([ ville.Dept,
                        ville.Nom,
                        ville.CP,
                        ville.Population,
                        ville.Surface,
                        ville.Longitude,
                        ville.Latitude,
                        ville.Alt_Max,
                        ville.Alt_Min
                      ])
    return liste_villes                      
                        
def convertir_str(liste_villes):
    '''
    convertit les valeurs de type int et float de chaque ville de la liste en str
    : param liste_villes
    type liste
    : effet de bord sur liste_villes
    '''
    for i in range(1, len(liste_villes)):  # on commence à la deuxième ligne, après la ligne des attributs
        liste_villes[i][2] = str(liste_villes[i][2] ) # cp
        liste_villes[i][3] = str(liste_villes[i][3] ) # population ...
        liste_villes[i][4] = str(liste_villes[i][4] ) # surf
        liste_villes[i][5] = str(liste_villes[i][5] ) # long
        liste_villes[i][6] = str(liste_villes[i][6] ) # latt
        liste_villes[i][7] = str(liste_villes[i][7] ) # altmin
        liste_villes[i][8] = str(liste_villes[i][8] ) # altmax
        
def convertir_csv(liste, separateur  = ';'):
    '''
    renvoie une liste de chaine compatible avec l'écriture d'un fichier en csv	
    : param : liste_villes une liste de villes
    type list
    : param  :separateur par défaut ';'
    type str
    return une liste de chaines
    type list
    '''
    liste_chaines = []
    for elt in liste:
        chaine = ''
        for i in range(len(elt)) :
            chaine += elt[i]
            if i < len(elt) - 1 : # si on n'est pas en bout de ligne, alors on ajoute un separateur
                chaine += separateur
        chaine += '\n' # caractère d"chapement fin de ligne
        liste_chaines.append(chaine) # on ajoute la chaine générée à la liste des chaines.
    return liste_chaines        

def sauver(collection, nom_fichier_CSV, separateur = ';'):
    """
    écrit dans le fichier csv les données de la collection
    : param collection
    type list of namedtuple
    : param nom_fichier_CSV
    type str
    : param separateur , le séparateur utilisé dans le fichier csv. par défaut ;
    type str
    : ne renvoie rien mais crée un fichier ou écrit dan sun fichier existant
    """
    assert type(nom_fichier_CSV) is str,'nom_fichier_CSV is str'
    liste = creer_liste_villes(collection) # crée une liste à partir de la collection
    convertir_str(liste) # convertit en str les valeurs numériques de la liste
    liste = convertir_csv(liste) # transforme la liste de listes en liste de chaines
    try :
        ecriture = open(nom_fichier_CSV, 'w', encoding = 'utf-8') # ouvre un canal en écriture vers fichier2
        ecriture.writelines(liste) # on écrit la liste de chaines
        ecriture.close() # on ferme le canal
    except:
        raise "!!!!!!!!!! impossible d'ecrire dans " + nom_fichier # affiche un msg d'erreur 
    
def creer_carte(collection_villes):
    '''
    en Utilisant folium, affiche les villes de la collection de villes
    : param collection_villes
    type list of namedtuples
    : Pas de return mais la création d'un fchier HTML dans le répertoire courant
    '''
    # par sécurité, on limite le nbre de villes à afficher à 400
    if len(collection_villes) > 400 :
        return 'trop de ville dans la liste'
    # on récupère dans la liste des villes la liste des coordonnées GPS
    coordonnees = [] # liste des futures coordonnées
    for ville in collection_villes :
        coordonnees.append((ville[6], ville[5])) # attention latitude puis longitude, c'est inversé dans la table
    # Création de la carte avec la première coordonnee    
    carte = folium.Map(location = coordonnees[0],zoom_start=6)
    #Ajout des autres coordonnées
    for coordonnee in coordonnees :
        folium.Map(location = coordonnee, zoom_start = 6).add_to(carte)
        folium.CircleMarker(coordonnee, radius = 3).add_to(carte)
    #sauvegarde de la carte au format HTML
    carte.save('carte_france.html')

def test() :
    liste_villes = lire_donnees('villes_metropole_corse.csv')
    collection_villes = creer_collection(liste_villes)
    c = renvoyer_villes(collection_villes,10)
    creer_carte(c)
    

################################################
###### doctest : test la correction desfonctions
################################################
if __name__ == "__main__": #si on est dans le prog principale on execute le doctest
    import doctest
    doctest.testmod(verbose = False) 