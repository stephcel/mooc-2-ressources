** Algorithmes de recherche textuelle **

Programmation dynamique
I. Algorithmes de recherche textuelle :

1) Présentation du problème :

a) Chaines de caractères :
Rappelons tout d’abord qu’une chaîne de caractères est un objet de type str composé de caractères.
Chaque caractère d’une chaîne est repéré par son index dans la chaîne, comme pour les listes. Les index
commencent à 0.Prenons l’exemple de la chaînes='abracadabra'. Alors s[0] a pour valeur le
caractère'a',s[3]a pour valeur le même caractère et s[4] le caractère'c'.La fonction len renvoie le
nombre de caractères d’une chaîne. Ici, len(s)a pour valeur 11.
Rappel sur le slicing :
La syntaxe globale d’une tranche est :
[𝑑é𝑏𝑢𝑡: 𝑓𝑖𝑛: 𝑝𝑎𝑠]
Chacun des arguments est optionnel. Attention, la valeur de fin n'est jamais comprise.
Pour davantage d’informations, vous pouvez consulter la page suivante (en anglais).

b) Recherche textuelle :
Considérons deux chaînes de caractères, l’une appelée texte, l’autre appelée motif, on cherche s’il existe
une occurrence du motif dans le texte, c’est-à-dire un index i tel que :
texte[i:i +len(motif)] == motif.
Plusieurs algorithmes ont été inventés pour résoudre ce problème, un des plus connus est l’algorithme de
Knuth, Morris, Pratt, qui nefigure pas au programme. Un autre algorithme, très efficace, est l’algorithme de
Boyer et Moore, qui a été inventé en 1977. Boyer et Moore travaillaient alors à l’université d’Austin au
Texas en tant qu’informaticiens. Boyer qui était aussi un mathématicien et maintenant à la retraite. Dans la
suite, on considère donc deux chaînes de caractères texte et motif, de longueurs respectives n et p. Bien
entendu, si 𝑝 > 𝑛, la recherche de motif dans texte échoue. En pratique on a 1 ≤ 𝑝 ≤ 𝑛, et
même 𝑝 beaucoup plus petit que 𝑛. Typiquement, on peut chercher un mot ou une phrase dans tout le texte
d’un roman.

c) Domaine d’application :
Un des secteurs qui utilise le plus cette recherche textuelle est le domaine de la bio-informatique notamment
dans l’analyse des informations génétiques. Cette information génétique présente dans nos cellules est portée
par les molécules d’ADN et permet la fabrication des protéines qui gère la quasi-totalité de nos fonctions
biologiques. La molécule d’ADN est constituée d’un grand nombre de nucléotides (environ 3,3 milliards de
paires de nucléotides) qui vont permettre la création d’acides aminés, composants des protéines. On compte
quatre sortes de nucléotides, appelés bases, symbolisés par les lettres A, C, G et T nommées respectivement
Adénine, Cytosine, Guanine et Thymine. Un nucléotide est une structure chimique composée d’une base
azotée, d’un phosphate et d’un sucre.
Notre ADN est composé de deux brins, plus exactement deux chaines complémentaires de nucléotides. Les
deux chaines sont complémentaires car il existe une complémentarité chimique qui fait que la Guanine fait
toujours face à la Cytosine et l’Adénine toujours face à la Thymine, à l’aide de liaisons moléculaires
appelées « liaisons hydrogènes » (3 pour le couple G-C et 2 pour le couple A-T).
On peut représenter ces deux brins par le schéma suivant :
NSI Tale
2
L’information génétique est donc très souvent représentée par de très longues chaines de caractères,
composées des caractères A, T, G et C.
Exemple : ATAACAGGAGTAAATAACGGCTGGAGTA…
Il est souvent nécessaire de détecter la présence de certains enchainements de bases azotées (dans la plupart
des cas un triplet de bases azotées). On se ramène donc ici au problème de recherche textuelle.

2) Algorithmes :

a) Algorithme naïf :
On parcours le texte intégralement, on compare le caractère courant avec le premier caractère du motif, puis
le suivant, etc. Si le motif complet est trouvé, on renvoie l'index du caractère courant, et sinon on renvoie -1
après avoir parcouru le texte complet.
Pseudo-code:
entier RechercheNaive (chaîne texte, chaîne motif)
Pour index allant de 0 à n-p
Faire
j←0
Tant que j<p et texte[index+j]=motif[j]
Faire
j←j+1
Si j=p
Alors Renvoyer index
FinPour
Renvoyer -1
Illustration:
Etape 1 :
texte : A T A A C A G G A G T A A A T A A C G G C T G G A G T A
motif : C G G C T G
Etape 2 :
texte : A T A A C A G G A G T A A A T A A C G G C T G G A G T A
motif : C G G C T G
[...]
Etape 5 :
texte : A T A A C A G G A G T A A A T A A C G G C T G G A G T A
motif : C G G C T G
[...]
Etape 18 :
NSI Tale
3
texte : A T A A C A G G A G T A A A T A A C G G C T G G A G T A
motif : C G G C T G

b) Algorithme de Boyer-Moore:
Cet algorithme, qui a été développé en 1977 par Robert S. Boyer et J. Strother Moore, repose sur les
caractéristiques suivantes :
• le motif est prétraité : l'algorithme connait alors tous les caractères du motif
• le décalage est judicieusement choisi : il peut être de plusieurs caractères
• les caractères sont comparés de droite à gauche
Reprenons le même exemple
Etape 1 :
texte : A T A A C A G G A G T A A A T A A C G G C T G G A G T A
motif : C G G C T G
Etape 2 :
texte : A T A A C A G G A G T A A A T A A C G G C T G G A G T A
motif : C G G C T G
A l'étape 1, on compare G et A qui sont différents. Comme la lettre A n'est pas présente dans le motif, on
peut décaler d'un motif entier. Il se passe exactement la même chose à l'étape 2.
Etape 3 :
texte : A T A A C A G G A G T A A A T A A C G G C T G G A G T A
motif : C G G C T G
A l'étape 3, on compare G et C qui sont différents. Cette fois-ci, le caractère C est présente dans le motif, on
se décale jusqu'au caractère C le plus à droite, soit de 2 caractères ici.
Etape 4 :
texte : A T A A C A G G A G T A A A T A A C G G C T G G A G T A
motif : C G G C T G
A l'étape 4, on a correspondance d'un caractère, mais pas du suivant. On décale donc jusqu'au prochain
caractère C.
Etape 5 :
texte : A T A A C A G G A G T A A A T A A C G G C T G G A G T A
motif : C G G C T G
On obtient une correspondance totale et on renvoie l'index 17.
Remarques :
• Plus le motif est long, plus cet algorithme est efficace.
• Le prétraitement consiste à garder dans une liste en mémoire la position de la dernière occurrence de
chaque caractère du motif. On utilise pour cela l'alphabet ASCII.
Liste Pretraitement(chaine motif)
Stock=Liste de 256 éléments initialisés à -1
Pour i variant de 0 à p-1
Faire
Stock[ord(motif[i])]i
FinPour
Retourner Stock
NSI Tale
4
Algorithme de Boyer-Moore:
Voici une implémentation de l’algorithme de Boyer-Moore. On renvoie ici non pas la première instance du
motif mais l’ensemble des instances du motif dans le texte sous forme de liste.
entier Boyer_Moore(motif, texte):
ListRes[]
DocPreTraitement(motif)
ntaille(texte)
ptaille(motif)
ip-1
ji
TantQue i<n :
Si texte[i]=motif[j] :
Si j=0 :
Ajouter i à ListRes
ii+2*p-1
jp-1
Sinon :
ii-1
jj-1
Sinon :
ii+p-1-minimum(j,Doc[ord(texte[i])])
jp-1
Retourner ListRes

II. Programmation dynamique:

1) Problème du rendu de monnaie:

a) Rappel du problème:
Nous avons déjà vu en classe de première qu’un algorithme glouton pouvait donner une réponse au
problème du rendu de monnaie. Nous revenons ici sur le contexte.
On suppose donné un système monétaire où les valeurs faciales des pièces (ou des billets) sont rangéesen
ordre décroissant. Par exemple, le système Euro pourra être décrit par la liste euros = [50, 20, 10, 5, 2, 1].
Pourpayer une somme de 48 unités on pourrait bien sûr payer 48 pièces de 1, ou encore 3 pièces de 10, 3
pièces de 5, 1 pièce de 2 et 1pièce de 1.On cherche à payer la somme indiquée, en supposant qu’on a autant
de pièces de chaque valeur que de besoin, en utilisant un nombre minimal de pièces.
L’algorithme glouton consiste à payer d’abord avec la plus grosse pièce possible : ici, il s’agit de 20, puisque
50 > 48. Ayant donné 20,il reste 28 à payer, et on poursuit avec la même méthode. Finalement, on va payer
48 sous la forme48=20+20+5+2+1. On aeu besoin de 5 pièces.
Considérons un autre système monétaire (en fait c’est l’ancien système impérial britannique) représenté par
la liste suivante de valeurs faciales imperial = [30, 24, 12, 6, 3, 1]. Pour payer 48, l’algorithme glouton va
répondre 48=30+12+6.À la différence du système euro, pour lequel on peut démontrer que l’algorithme
glouton donne toujours la réponse optimale, on constate que ce n’est pas le cas avec le système impérial,
puisque on aurait pu se contenter de 2 pièces :48=24+24. L'euro est un système de monnaie dit canonique
(dans ce cas, l'algorithme glouton est optimal) tandis que le système de monnaie impérial ne l'est pas (et
donc l'algorithme glouton ne donne pas nécessairement une solution optimale).
Rappelons comment peut se programmer l’algorithme glouton :
NSI Tale
5

b) Recherche de la réponse optimale :
La réponse optimale est celle qui utilise le nombre minimal de pièces. Nous avons vu que l’algorithme
glouton échoue à la trouver en général. Une approche récursive permet de résoudre le problème : soit𝑥une
valeur faciale de l’une des pièces du système monétaire. Pour rendre une somme 𝑆 de façon optimale, si l’on
veut utiliser au moins une fois la pièce𝑥, il suffit de rendre 𝑥 et la somme 𝑆 − 𝑥 de façon optimale. Il n’y a
plus qu’à choisir, parmi tous les choix possibles de𝑥, celui qui permet d’utiliser le minimum de pièces.
Autrement dit, si on appelle𝑓(𝑆)le nombre minimal de pièces qu’il faut utiliser pour payer la somme 𝑥, on a
simplement𝑓(0) = 0 et 𝑓(𝑠) = min
𝑥⩽𝑠 (1 + 𝑓(𝑠 − 𝑥)), le minimum étant calculé sur toutes les valeurs 𝑥 d’une
pièce. Cette remarque permet d’écrire le programme récursif suivant :
On a importé du module math la valeur spéciale inf qui représente l’infini : pour tout entier a,
l’expression 𝑎 < 𝑖𝑛𝑓vaut 𝑇𝑟𝑢𝑒.
Exercice :
Ecrire la fonction précédente et tester son exécution sur le système Euro et 𝑆 = 48. Que se passe-t-il?
Comment l'expliquer?
Hélas, l’exécution de l’appel dynRecursif(euros, 48)est extrêmement lente, les mêmes calculs étant
effectués de façon répétée. Une analyse du problème montrerait que la complexité est exponentielle.
Une idée classique consiste à mémoriser les résultats des appels pour être sûr qu’on n’aura pas besoin de les
calculer plusieurs fois. Ici, le calcul de𝑓(𝑆)utilise le calcul de𝑓(𝑆 − 𝑥)pour chaque valeur de𝑥. Autrement
dit,𝑓(𝑆)n’utilise au plus que les valeurs de𝑓(𝑆 − 1), 𝑓(𝑆 − 2), . . . , 𝑓(3), 𝑓(2), 𝑓(1).On va donc créer un
tableau conservant ces données, et calculer de façon systématique pour des valeurs croissantes de l’index :
Le calcul de 𝑓(48) va peut-être utiliser plusieurs fois la valeur de 𝑓(8), mais celle-ci n’aura cette fois été
calculée qu’une seule fois,et aussitôt rangée en mémoire. Cela ne fonctionne que parce que pour calculer
𝑓(48), par exemple, on a recours seulement aux valeurs de f(s) pour s < 48.
Cette technique est habituellement appelée mémoïsation, fort laide tentative de traduction de l’anglais
memoization mais qui est passée dans l’usage.
L’algorithme est de complexité tout à fait raisonnable : les deux boucles emboîtées correspondent à un
temps d’exécution de l’ordre de 𝑠𝑜𝑚𝑚𝑒 ∗ 𝑙𝑒𝑛(𝑠𝑦𝑠𝑡è𝑚𝑒_𝑚𝑜𝑛𝑛𝑎𝑖𝑒).
NSI Tale
6
En revanche, il a un coût en espace (ou en mémoire) : il faut réserver la place nécessaire pour ranger toutes
les valeurs de 𝑓(𝑛).
c) Reconstitution des détails de la réponse optimale :
Si on remplace la dernière ligne de la fonction dynMemoise par return f, l’appel dynMemoise(17,
imperial) renvoie le tableau [0, 1, 2, 1, 2, 3, 1, 2, 3, 2, 3, 4, 1, 2, 3, 2, 3, 4].
Il y a une réponse optimale avec 4 pièces, comment la reconstituer ?
On a 𝑓(17) = 4, on cherche une pièce 𝑥 telle que 𝑓(17 − 𝑥) = 3, on a le choix entre 𝑥 = 1, 𝑥 = 3, et
𝑥 = 12. Choisissons 𝑥 = 12.
On a 𝑓(17) = 1 + 𝑓(5). On cherche maintenant une pièce 𝑥. telle que 𝑓(5 − 𝑥) = 2, on peut choisir
𝑥 = 3 (𝑥 = 1 aurait également convenu). On a 𝑓(17) = 1 + 𝑓(5) = 1 + 1 + 𝑓(2) et enfin 𝑓(2) =
1 + 𝑓(1). Une solution optimale est donc de payer17 = 1 + 1 + 3 + 12, avec 4 pièces.
On peut modifier la fonction précédente pour reconstituer une décomposition optimale : il suffit de détailler
le calcul du minimum et en profiter pour mémoriser les valeurs notées x ci-dessus.

2) Programmation dynamique :
On peut dire que la résolution algorithmique d’un problème relève de la programmation dynamique si
• le problème peut être résolu à partir de sous-problèmes similaires mais plus petits ;
• l’ensemble de ces sous-problèmes est discret, c’est-à-dire qu’on peut les indexer et ranger les
résultats dans un tableau ;
• la solution optimale au problème posé s’obtient à partir des solutions optimales des sous-problèmes ;
• les sous-problèmes ne sont pas indépendants et un traitement récursif fait apparaître les mêmes sous-
problèmes un grand nombre de fois.
En général, c’est trouver une équation récursive, comme on l’a fait en 1.b) pour le problème du rendu de
monnaie, qui constitue l’étape essentielle de la résolution du problème.
Les algorithmes gloutons, en revanche, ne fournissent pas toujours une solution optimale, il décompose le
problème initial à l’aide d’un seul sous-problème.
Les algorithmes nombreux sont souvent moins coûteux en exécution, mais sans pouvoir garantir une
solution optimale.
Dans les deux cas, une propriété importante à vérifier est que l’optimalité d’une solution au problème soit
garantie par l’optimalité des solutions aux sous-problèmes.
NSI Tale
7

3) Problème du sac à dos:
On dispose de 𝑛 objets de poids entiers strictement positifs 𝑝0, 𝑝1, … , 𝑝𝑛−1 tels que 𝑝0 ≤ 𝑝1 ≤ ⋯ ≤ 𝑝𝑛−1, et
auxquels on attache une valeur, qui est également un entier strictement positif. On note 𝑣0, 𝑣1, … , 𝑣𝑛−1 les
valeurs de ces objets.
On dispose d’un sac qu’on ne doit pas surcharger : le poids total des objets qu’il contient ne doit pas
dépasser un certain poids 𝑃.
On cherche à maximiser le total des valeurs des objets du sac.
Autrement dit, on cherche des nombres 𝑥𝑖 valant 0 ou 1 tels que ∑ 𝑥𝑖𝑝𝑖
𝑛−1
𝑖=0 ≤ 𝑃tout en maximisant∑ 𝑥𝑖𝑣𝑖
𝑛−1
𝑖=0 .
Exemple :
Poids 1 2 5 6 7
Valeurs 1 6 18 22 28
𝑃 = 11.
En choisissant les objets de poids 1, 2 et 7, obtenant une valeur égale à 1 + 6 + 28 = 35.
On peut aussi choisir les objets de poids 5 et 6, obtenant une valeur égale à 18+22 = 40.
Mais comment s’assurer qu’on ne peut pas mieux faire ?
Notons 𝑉 (𝑖, 𝑝)pour 0 ≤ 𝑖 ≤ 𝑛 − 1 et 0 ≤ 𝑝 ≤ 𝑃 la valeur maximale des objets qu’on peut ranger parmi
ceux de numéros 0 à 𝑖 sans dépasser le poids 𝑝. La valeur maximale que nous cherchons est alors 𝑉 (𝑛 −
1, 𝑃).
Bien sûr, pour tout 𝑖, 𝑉 (𝑖, 0) = 0 et 𝑉 (0, 𝑝) = { 0 𝑠𝑖 𝑝 < 𝑝0
𝑣0 𝑠𝑖 𝑝 ≥ 𝑝0 .
La clé de la résolution tient à l’équation récursive :
𝑉 (𝑖, 𝑝) = { 𝑉 (𝑖 − 1; 𝑝) 𝑠𝑖 𝑝 < 𝑝𝑖
𝑚𝑎𝑥(𝑉 (𝑖 − 1, 𝑝) , 𝑣𝑖 + 𝑉 (𝑖 − 1; 𝑝 − 𝑝𝑖))𝑠𝑖 𝑝 ≥ 𝑝𝑖
On a distingué deux cas :
• On renonce à utiliser l’objet numéro 𝑖 : la valeur maximale est alors 𝑉 (𝑖; 𝑝).
• Si on utilise l’objet numéro 𝑖, on doit limiter à 𝑝 − 𝑝𝑖 le poids consacré aux objets de numéros 0 à
𝑖 − 1 et ajouter sa valeur 𝑣𝑖 à la valeur optimale du sous-problème.
Programme Python :
On obtient alors :
L’imbrication des deux boucles permet d’assurer un temps d’exécution de l’ordre de 𝑛 × 𝑃. Le coût en
mémoire est également del’ordre de 𝑛 × 𝑃, puisqu’il faut réserver la place du tableau 𝑉.
En fait, on pourrait consommer moins de mémoire. En effet on s’aperçoit que le calcul de la ligne 𝑉[𝑖] du
tableau utilise seulement les valeurs de la ligne précédent 𝑉[𝑖 − 1] et que la mise à jour de 𝑉[𝑖][𝑝] n’utilise
que les valeurs de 𝑉[𝑖 − 1][𝑘] pour𝑘 ≤ 𝑝.
NSI Tale
8
Ainsi peut-on réécrire le programme de la façon suivante, en n’utilisant qu’une ligne de mémoire pour le
tableau 𝑉.