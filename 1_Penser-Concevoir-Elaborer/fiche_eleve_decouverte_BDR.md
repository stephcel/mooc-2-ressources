# Traitement des données en table

## Introduction

Nous avons vu que les sites web sont référencés dans des bases de données qu'utilisent le moteurs de recherche lorsque vous faites des requêtes, que vos données personnelles sont collectées en masse à chacun de vos _clics_ lorsque vous naviguez sur la toile, utilisez les réseaux sociaux, commandez un article en ligne... 

Les bases de données, si elles ont parfois un côté dérangeant, sont indispensables à la recherche scientifiques (entre autre) qui s'appuie sur elles la plupart du temps. Les avancées réalisées dans le domaine de l'IA, par exemple, n'existeraient pas sans elles.

**Les données sont partout, en quantité astronomique !!**

Quand on voit la difficulté qu'on peut avoir à gérer un simple tiroir mal rangé et rempli de chaussettes dépareillées, on peut se poser deux questions :

* Comment stocker/ranger ces données de manière efficace ?
* Comment les utiliser tout aussi efficacement ?

Il existe de nombreuses façon de faire cela. En terminale, en NSI, nous parlerons du langage SQL. En seconde, nous allons nous intéresser aux **données en table** que l'on stocke dans un tableau.

## Vocabulaire

Regrouper des données de manière organisée (donc structurée) permet d'en faciliter l'exploitation et permet de produire de nouvelles informations. 

Ainsi, une voiture peut se caractériser par les données "VW", "POLO", "ESSENCE", "95600", "2013" pour décrire respectivement la marque, le modèle, l'énergie, le kilométrage et l'année de mise en circulation. Ces caractéristiques sont appelées **attributs** (ou descripteur).  Les données **qualitatives** se présentent sous forme de chaines de caractères, les données **quantitatives**, sous forme de nombres.

| Marque  | Modèle | Energie     | Km    | Année |
| ------- | ------ | ----------- | ----- | ----- |
| VW      | Polo   | Essence     | 95600 | 2013  |
| Renault | Zoé    | Electricité | 12300 | 2017  |
| Peugeot | 308    | Diesel      | 56700 | 2015  |

Le tableau ci-dessus utilise 5 **attributs** pour décrire des véhicules.  Ces véhicules partageant les mêmes **attributs**, constituent une **relation** (ou **collection**).

Une **relation** est généralement représentée sous forme d'une table (ou tableau) :

* Chaque ligne de la table correspond à un objet : c'est un **enregistrement**.
*  l'intersection d'une ligne (enregistrement) et d'une colonne (attribut) est une **donnée** ou **valeur**.

## Le format CSV

Le format **csv**,  pour **c**_omma_ **s**_eparated_ **v**_alues_, est un **format texte** dans lequel les données s'écrivent de la façon suivante :

* Les enregistrements (lignes) sont séparés par un retour chariot (aller à la ligne)
* les données sont séparées par un `;` ou une `,`.
* Dans la première ligne, on place les attributs en utilisant le même séparateur (`;` ou `,` )que pour les données.

> * Écrivez la relation précédente en utilisant ce format dans un éditeur de texte (notepad++, geany ...)
> * Sauvegardez le fichier au format `csv`.
> * Ouvrez le fichier avec un tableur (libreoffice calc).

## QUIZZ

Nous allons les structurer dans un fichier csv en y regroupant les questions de tous les élèves de la classe.

> * Créez un fichier _nom_prenom.csv_ dans lequel vous structurerez vos questions avec pour attributs _question_, _bonne_reponse_, _reponse_2_,_reponse_3_ et _reponse_4_. 
> * Dans le disque commun de votre classe enregistrez votre fichier dans le repertoire spécialemnt créé à cet effet.
> * Créez maintenant sur votre espace personnel un fichier _quizz.csv_ regroupant les questions de tous les groupes de la classe.
> * Ouvrez ce fichier avec un tableur.

## Importer des données en Python

Nous allons importer toutes les lignes du fichier texte dans un tableau Python. Il faudra :

* enlever les chaines `\n` qu'on trouvera à chaque fin de ligne (cf [caractères d'échappement](https://fr.wikipedia.org/wiki/Caract%C3%A8re_d%27%C3%A9chappement))
* scinder chaque ligne à chaque `;` ou `,` selon le séparateur que vous avez choisi.

On obtiendra alors un tableau dont chaque élément est un autre tableau correspondant à une ligne de données.

Le code nécessaire étant un peu complexe, le voici. Regardez bien chaque ligne pour en comprendre le fonctionnement.

```python
def lire_donnees(nom_fichier, separateur = ';'):
    """
        lit le fichier csv dans l'URL est spécifiée en paramètre et revoie un tableau des donnees
        : params
            nom_fichier (str)
            separateur (str) par défaut ;
        return tableau des données (list)
    """
    assert type(nom_fichier) is str,'nom_fichier is str'
    try :
        lecture = open(nom_fichier,'r',encoding = 'utf_8') # ouvre un canal en lecture vers text.txt
    except FileNotFoundError :
        raise # renvoie une erreur si le ficier n'existe pas
    toutes_les_lignes = lecture.readlines() # renvoie toutes les lignes du fichier dans la liste lignes
    for i in range(0,len(toutes_les_lignes)): 
        toutes_les_lignes[i] = toutes_les_lignes[i].rstrip("\n").split(separateur) # enleve les "\n" et convertit en list
    lecture.close() # ferme le canal en lecture
    return toutes_les_lignes

```

> * Codez le module _module_csv.py_ qui contient uniquement cette fonction
>
> * Depuis la console importez ce module puis, grâce à lui, importez dans un tableau nommé _QUIZZ_ les données de vote fichier _quizz.csv_.
> * A quoi correspond `QUIZZ[0]` ? 
> * A quoi correspond `QUIZZ[1][0] `?
> * Comment obtenir la bonne réponse à la troisième question du quizz ?
> * Modifiez votre projet _Quizz_ en important dans le tableau QUIZZ les données du fichier csv. Pensez à supprimer la ligne des attributs.

## Sauvegarder une relation

Pour sauvegarder au format csv les données stockées dans un tableau Python, il faut :

* Créer une chaine de caractères contenant les données du tableau en utilisant le format csv
* écrire dans un fichier texte cette chaine de caractères.

Voici la fonction de conversion. Ajoutez la dans votre module :

``` python
def convertir_csv(tab, separateur  = ';'):
    '''
    renvoie une tableau de chaines compatible avec l'écriture d'un fichier en csv	
    : params
        tab (list)
        separateur (str) par défaut ';'
    : return un tableau de chaines (tab)
    '''
    tab_chaines = []
    for elt in tab:
        chaine = ''
        for i in range(len(elt)) :
            chaine += elt[i]
            if i < len(elt) - 1 : # si on n'est pas en bout de ligne, alors on ajoute un separateur
                chaine += separateur
        chaine += '\n' # caractère d"chapement fin de ligne
        tab_chaines.append(chaine) # on ajoute la chaine générée à la liste des chaines.
    return tab_chaines
```

> Depuis la console, testez cette fonction avec le tableau QUIZZ 

Voici la fonction de sauvegarde en csv. Ajoutez là à votre module.

```python
def sauver(relation, nom_fichier, separateur = ';'):
    """
    écrit dans le fichier csv les données de la collection
    : params
        relation (list) le tableau contenant les données de la relation
        nom_fichier (str)
        separateur (str) le séparateur utilisé dans le fichier csv. par défaut ;
    : ne renvoie rien mais crée un fichier ou écrit dan sun fichier existant
    """
    assert type(nom_fichier) is str,'nom_fichier_CSV is str'
    tab_csv = convertir_csv(relation)
    try :
        ecriture=open(nom_fichier,'w',encoding='utf-8') # ouvre un canal en écriture vers fichier2
        ecriture.writelines(tab_csv) # on écrit la liste de chaines
        ecriture.close() # on ferme le canal
    except:
        raise "!!!!!!!!!! impossible d'ecrire dans " +nom_fichier # affiche un msg d'erreur 
```